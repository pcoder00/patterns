export abstract class Pizza {
    protected description: string = "";

    public getDescription() {
        return this.description;
    }

    public prepare() {

    }

    public bake() {

    }

    public cut() {

    }

    public box() {

    }
}
