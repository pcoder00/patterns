import {Pizza} from "../Pizza/Pizza";

export declare type NullablePizza = Pizza | null;

export abstract class PizzaStore {
    public orderPizza(type: string): Pizza {
        let pizza: Pizza = this.createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    protected abstract createPizza(type: string): Pizza
}
