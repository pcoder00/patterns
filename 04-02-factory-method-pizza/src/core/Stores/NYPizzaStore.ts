import {NullablePizza, PizzaStore} from "./PizzaStore";
import {Pizza} from "../Pizza/Pizza";
import {NYCheesePizza} from "../Pizza/NY/NYCheesePizza";
import {NYPepperoniPizza} from "../Pizza/NY/NYPepperoniPizza";


export class NYPizzaStore extends PizzaStore {
    protected createPizza(type: string): Pizza {
        let pizza: NullablePizza = null;
        if (type === "cheesy") {
            pizza = new NYCheesePizza();
        }

        if (type === "pepperoni") {
            pizza = new NYPepperoniPizza();
        }
        if (!pizza) {
            throw Error("no such pizza");
        }


        return pizza;
    }
    
}
