import {ChicagoPizzaStore} from "./core/Stores/ChicagoPizzaStore";
import {Pizza} from "./core/Pizza/Pizza";
import {NYPizzaStore} from "./core/Stores/NYPizzaStore";

let chicagoStore = new ChicagoPizzaStore();

let chicagoCheesy: Pizza =  chicagoStore.orderPizza("cheesy");
let chicagoPepperoni: Pizza =  chicagoStore.orderPizza("pepperoni");
let chicagoClam: Pizza =  chicagoStore.orderPizza("clam");
console.dir({chicagoCheesy});
console.dir({chicagoPepperoni});
console.dir({chicagoClam});

let nyStore = new NYPizzaStore();

let nyCheesy: Pizza = nyStore.orderPizza("cheesy");
let nyPepperoni: Pizza = nyStore.orderPizza("pepperoni");
let nyClam: Pizza = nyStore.orderPizza("clam");

console.dir({nyCheesy});
console.dir({nyPepperoni});
console.dir({nyClam});
