import {NullablePizza, PizzaStore} from "./PizzaStore";
import {Pizza} from "../Pizza/Pizza";
import {CheesePizza} from "../Pizza/CheesePizza";
import {PepperoniPizza} from "../Pizza/PepperoniPizza";
import {PizzaIngredientsFactory} from "../Ingredients/Factories/PizzaIngredientFactory";
import {NYPizzaIngredientFactory} from "../Ingredients/Factories/NYPizzaIngredientFactory";
import {ClamPizza} from "../Pizza/ClamPizza";

export class NYPizzaStore extends PizzaStore {
    protected createPizza(type: string): Pizza {
        let pizza: NullablePizza = null;
        let pizzaIngredientFactory: PizzaIngredientsFactory = new NYPizzaIngredientFactory();
        return super.pizzaSelector(type, pizzaIngredientFactory);
    }
    
}
