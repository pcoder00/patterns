import {NullablePizza, PizzaStore} from "./PizzaStore";
import {Pizza} from "../Pizza/Pizza";
import {CheesePizza} from "../Pizza/CheesePizza";
import {PepperoniPizza} from "../Pizza/PepperoniPizza";
import {PizzaIngredientsFactory} from "../Ingredients/Factories/PizzaIngredientFactory";
import {ChicagoPizzaIngredientFactory} from "../Ingredients/Factories/ChicagoPizzaIngredientFactory";
import {ClamPizza} from "../Pizza/ClamPizza";


export class ChicagoPizzaStore extends PizzaStore {
    protected createPizza(type: string): Pizza {
        let pizza: NullablePizza = null;
        let pizzaIngredientFactory: PizzaIngredientsFactory = new ChicagoPizzaIngredientFactory();
        return super.pizzaSelector(type, pizzaIngredientFactory);
    }
    
}
