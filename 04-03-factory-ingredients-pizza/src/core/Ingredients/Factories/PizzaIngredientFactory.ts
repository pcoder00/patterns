import {Dough} from "../Dough/Dough";
import {Sauce} from "../Sauces/Sauce";
import {Cheese} from "../Cheese/Cheese";
import {Veggie} from "../Veggies/Veggie";
import {Pepperoni} from "../Pepperoni/Pepperoni";
import {Clams} from "../Clams/Clams";

export interface PizzaIngredientsFactory {
    createDough(): Dough;
    createSauce(): Sauce;
    createCheese(): Cheese;
    createVeggies(): Veggie[];
    createPepperoni(): Pepperoni;
    createClams(): Clams;
}
