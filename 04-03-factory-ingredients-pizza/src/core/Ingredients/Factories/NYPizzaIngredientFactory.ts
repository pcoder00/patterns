import {PizzaIngredientsFactory} from "./PizzaIngredientFactory";

import {Cheese} from "../Cheese/Cheese";
import {Dough} from "../Dough/Dough";
import {Clams} from "../Clams/Clams";
import {Pepperoni} from "../Pepperoni/Pepperoni";
import {Sauce} from "../Sauces/Sauce";
import {Veggie} from "../Veggies/Veggie";

import {ThinCrustDough} from "../Dough/ThinCrustDough";
import {MarinaraSauce} from "../Sauces/MarinaraSauce";
import {ReggianoCheese} from "../Cheese/ReggianoCheese";
import {Garlic} from "../Veggies/Garlic";
import {Onion} from "../Veggies/Onion";
import {Mushroom} from "../Veggies/Mushroom";
import {RedPepper} from "../Veggies/RedPepper";
import {FreshClams} from "../Clams/FreshClams";
import {SlicedPepperoni} from "../Pepperoni/SlicedPepperoni";

export class NYPizzaIngredientFactory implements PizzaIngredientsFactory {

    createDough(): Dough {
        return new ThinCrustDough();
    }

    createSauce(): Sauce {
        return new MarinaraSauce();
    }

    createCheese(): Cheese {
        return new ReggianoCheese();
    }

    createVeggies(): Veggie[] {
        return [new Garlic(), new Onion(), new Mushroom(), new RedPepper()];
    }

    createClams(): Clams {
        return new FreshClams();
    }

    createPepperoni(): Pepperoni {
        return new SlicedPepperoni();
    }



}
