import {Dough} from "../Ingredients/Dough/Dough";
import {Sauce} from "../Ingredients/Sauces/Sauce";
import {Veggie} from "../Ingredients/Veggies/Veggie";
import {Cheese} from "../Ingredients/Cheese/Cheese";
import {Pepperoni} from "../Ingredients/Pepperoni/Pepperoni";
import {Clams} from "../Ingredients/Clams/Clams";

export abstract class Pizza {
    protected name: string = "";
    protected dough: Dough;
    protected sauce: Sauce;
    protected veggies: Veggie[];
    protected cheese: Cheese;
    protected pepperoni: Pepperoni;
    protected clams: Clams;

    public abstract prepare(): void;

    public bake(): void {
        console.log("Bake for 25 minutes at 350");
    }

    public cut(): void {
        console.log("Cutting the pizza into diagonal slices");
    }

    public box(): void {
        console.log("Place pizza in official PizzaStore box");
    }

    protected setName(name: string) {
        this.name = name;
    }

    public getName(): string {
        return this.name;
    }



}
